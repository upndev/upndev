<?php

$myip = '77.240.253.82';

if($_SERVER['REMOTE_ADDR'] != $myip)
{
	die('access forbidden');
}

$seo_content = array(
        "title" => "Upndev",
        "description" => "Intelligent solutions to optimize your business",
        "keywords" => "Websites, Landing Pages, Facebook Apps, Web Apps, SaaS, Front-end, Back-end, Chatbots",
        "img_url" => "img\seo\upndev.jpg"
)





?>

<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">
<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!-- title -->
    <title><?php echo $seo_content['title']?></title>
    <meta name="title" content="<?php echo $seo_content['title']?>">
    <meta property="og:title" content="<?php echo $seo_content['title']?>">
    <meta itemprop="name" content="<?php echo $seo_content['title']?>">
    <meta name="application-name" content="<?php echo $seo_content['title']?>">
    <meta name="apple-mobile-web-app-title" content="<?php echo $seo_content['title']?>">
    <!-- description -->
    <meta property="og:description" content="<?php echo $seo_content['description']?>">
    <meta itemprop="description" content="<?php echo $seo_content['description']?>">
    <meta name="description" content="<?php echo $seo_content['description']?>">
    <!-- keywords -->
    <meta name="keywords" content="<?php echo $seo_content['keywords']?>">
    <!-- image -->
    <meta itemprop="image" content="<?php echo $seo_content['img_url']?>">
    <meta property="og:image" content="<?php echo $seo_content['img_url']?>">
    <meta name="twitter:image" content="<?php echo $seo_content['img_url']?>">
    <!-- language -->
    <meta name="language" content="en">
    <meta name="dc.language" content="en">
    <!-- color -->
    <meta id="theme-color" name="theme-color" content="#008cff">
    <meta name="apple-mobile-web-app-status-bar-style" content="#008cff">
    <meta name="msapplication-navbutton-color" content="#008cff">
    <!-- icons -->
    <link rel="shortcut icon" href="/themes/builder/assets/icons/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="/manifest.json">


    <link rel="apple-touch-icon" sizes="57x57" href="/themes/builder/assets/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/builder/assets/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/builder/assets/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/builder/assets/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/builder/assets/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/builder/assets/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/builder/assets/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/builder/assets/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/builder/assets/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/builder/assets/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/builder/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/builder/assets/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/builder/assets/icons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#fb403c">
    <meta name="msapplication-TileImage" content="/themes/builder/assets/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#fb403c">


    <link href="/themes/builder/assets/icons/apple-icon-114x114.png" sizes="114x114" rel="apple-touch-icon-precomposed">
    <link href="/themes/builder/assets/icons/apple-icon-72x72.png" sizes="72x72" rel="apple-touch-icon-precomposed">
    <link href="/themes/builder/assets/icons/apple-icon-57x57.png" sizes="57x57" rel="apple-touch-icon-precomposed">


    <link rel="shortcut icon" sizes="196x196" href="/themes/builder/assets/icons/icon-196x196.png">
    <link rel="shortcut icon" sizes="128x128" href="/themes/builder/assets/icons/icon-128x128.png">
    <link rel="logo" type="image/svg" href="/themes/builder/assets/icons/logo.svg">
    <!-- bots -->
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <!-- other -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google" content="notranslate">
    <meta name="author" content="Upndev">
    <meta name="copyright" content="Upndev">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tooltip" content="Launch Upndev!">
    <meta name="msapplication-tap-highlight" content="no">
    <meta http-equiv="window-target" content="_top">
    <meta name="googlebot" content="noodp">

	<?php /*
	<link rel="icon" href="img/favicon.ico">
	<link rel="canonical" href="http://www.example.com/">
	<meta name="keywords" content="keywords">
	<meta name="description" content="description">
	<meta name="author" content="author">
	<!-- place -->
	<meta name="geo.region" content="LT-VL">
	<meta name="ICBM" content="54.687156, 25.279651">
	<meta name="geo.placename" content="Vilnius">
	<meta name="geo.position" content="54.687156;25.279651">
	<meta name="dc.language" content="lt">
	<!-- itemprop -->
	<meta itemprop="name" content="Example">
	<meta itemprop="description" content="description">
	<meta itemprop="image" content="http://www.example.com/images/example.jpg">
	<!-- image -->
	<meta property="og:image" content="http://www.example.com/images/example.jpg">
	<meta property="og:image:secure_url" content="https://www.example.com/images/example.jpg">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">
	<!-- og main -->
	<meta property="fb:app_id" content="33333333">
	<meta property="og:title" content="Example">
	<meta property="og:description" content="description">
	<meta property="og:type" content="profile">
	<meta property="og:site_name" content="Example Parent">
	<meta property="og:locale" content="lt_LT">
	<meta property="og:locale:alternate" content="en_US">
	<meta property="og:locale:alternate" content="ru_RU">
	<meta property="og:url" content="http://www.example.com/">
	<!-- video -->
	<meta property="og:video" content="http://example.com/movie.swf" />
	<meta property="og:video:secure_url" content="https://secure.example.com/movie.swf" />
	<meta property="og:video:type" content="application/x-shockwave-flash" />
	<meta property="og:video:width" content="400" />
	<meta property="og:video:height" content="300" />
	<!-- audio -->
	<meta property="og:audio" content="http://example.com/sound.mp3" />
	<meta property="og:audio:secure_url" content="https://secure.example.com/sound.mp3" />
	<meta property="og:audio:type" content="audio/mpeg" />
	<!-- twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@mytwitter" />
	<meta name="twitter:title" content="Example" />
	<meta name="twitter:description" content="description" />
	<meta name="twitter:image" content="https://www.example.com/images/example.jpg" />
	<meta name="twitter:url" content="https://www.example.com/" />
	<!-- apple -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.png"> <!-- 60 x 60 -->
	<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	<link rel="apple-touch-startup-image" href="startup.png" /> <!-- 320 x 480 -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!-- android -->
	<link rel="shortcut icon" sizes="196x196" href="touch-icon-android-retina.png">
	<link rel="shortcut icon" sizes="128x128" href="touch-icon-android.png">
	*/ ?>
	<script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retinaimages.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
	<noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("/retinaimages.php?devicePixelRatio=2")}</style></noscript>
	<?php
	$dev = strpos($_SERVER['HTTP_HOST'], 'upndev.com') === false;
	if ($dev) {
		require 'vendor/less-compiler/Less.php';
		require 'vendor/replacer.php';
		$content = file_get_contents('less/frontend.less');
		$parser = new Less_Parser([
			'compress'=>true,
			'sourceMap'=>true,
			'sourceMapWriteTo'=>'css/frontend.min.map',
			'sourceMapURL'=>'css/frontend.min.map',
		]);
		//$content = Replacer::get($content);
    	$parser->parse($content);
		$parser->ModifyVars([
			'dir' => '"'.__DIR__.'/../less/"',
			'url' => '"../"'
		]);
		$css = $parser->getCss();
		file_put_contents('css/frontend.min.css',$css);
	}
	?>
	<link rel="stylesheet" type="text/css" href="css/frontend.min.css">
</head>
<body>
<header class="header">
    <a href="#" data-link="hero" class="js-logo" id="logoLink"></a>
    <a href="#" class="js-menu" id="menuLink"></a>
    <nav class="navigation">
        <ul class="navigationBlock">
            <li class="navigationItem"><a data-link="about" href="#" class="navigationLink">About us</a></li>
            <li class="navigationItem"><a data-link="services" href="#" class="navigationLink">Services</a></li>
            <li class="navigationItem"><a data-link="projects" href="#" class="navigationLink">Projects</a></li>
            <li class="navigationItem"><a data-link="footer" href="#" class="navigationLink">Contacts</a></li>
        </ul>
    </nav>
</header>