<footer class="footer">
    <h2 class="sectionHeading">Contacts</h2>
    <a href="mailto:info@upndev.com" class="footerLinks email">info@upndev.com</a>
    <div class="phoneBlock">
        <a href="tel:+37061615708" class="footerLinks">+370 616 15708</a>
        <a href="tel:+37061615709" class="footerLinks">+370 616 15709</a>
    </div>

</footer>
	<script async type="text/javascript" src="js/plugins.min.js"></script>
</body>
</html>