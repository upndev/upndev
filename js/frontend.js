var swiper, x, ofs = 0, link, mq;


$(function () {
	$b = $('body');



    //------------MENU SCROOL & FUNCTIONALITY---------------
    $('.navigationLink, .heroBtn, .js-logo').click(function (e) {

        e.preventDefault();
        link = $(this).data('link');
        $('.navigationLink').removeClass('isClicked');
        $b.removeClass('nav-isActive');
        $(this).addClass('isClicked');


        if (mq.matches) {
            ofs = link === 'projects'?   $('.projects').outerHeight()*0.11 * -1 : 0;

        } else  {
           ofs = link === 'projects'?  $('.projects').outerHeight()*0.12 : 0;


        }

        console.log(ofs);



        $("html, body").animate({scrollTop: $('.' + link).offset().top + ofs +1}, 600);
    });



    $('.js-menu').on('click',function (e) {
        e.preventDefault();
		$('body').toggleClass('nav-isActive');
    });

    //----------------SWIPER INIT-------------
    swiper = new Swiper('.swiper-container', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        loop: true,
        speed: 500,
        autoplay: {
            delay: 3000,
        },
    });



    // media query event handler
    if (matchMedia) {
        mq = window.matchMedia("(min-width: 750px)");
        mq.addListener(WidthChange);
        WidthChange(mq);
    }


    // media query change
    function WidthChange(mq) {
        if (mq.matches) {
        	console.log('desktop');
            setLayouts('desktop');
            $b.removeClass('mobile')


        } else {
            console.log('mobile');
            setLayouts('mobile');
            $b.addClass('mobile')


        }

    }

    function setLayouts(t) {
        if (t === 'mobile') {
            $('.hero').ClipPath('0% 0%, 40.53vw 0, 100% 65.06vw, 100% 100%, 0% 100%');
            $('.layout--right').ClipPath('0% 0%, 100% 0, 100% 100%, 0% 185.33vw, 0% 0%');
            $('.layout--center').ClipPath('0% 49.07%, 100% 78.1%, 100% 100%, 60% 100%, 0% 82.65%');
            $('.layout--left').ClipPath('0% 0%, 60% 100%, 0% 100%');
        } else if (t === 'desktop'){
            $('.hero').ClipPath('0% 0%, 60% 0, 100% 80%, 100% 100%, 0% 100%');
            $('.layout--right').ClipPath('0% 0%, 100% 0, 100% 100%, 0% 10%, 0% 0%');
            $('.layout--center').ClipPath('0% 11%, 85.5% 100%, 52.11% 100%, 0% 46.1%, 0% 0%');
            $('.layout--left').ClipPath('0% 0%, 52% 100%, 0% 100%');
        }
    }





});





