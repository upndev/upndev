<?php include 'includes/header.php'; ?>


<div class="hero--noClipPath"></div>
<section class="hero">
	<div class="heroBlock">
        <svg id="heroLogo" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 365.7 99.7" enable-background="new 0 0 365.7 99.7" xml:space="preserve"><g><path fill="#FFFFFF" d="M305.9,57.1h-41.2c1.5,6.7,6,10.9,12.1,10.9c4.1,0,9-1,12-6.5l15.9,3.2c-4.8,11.9-15,17.6-27.9,17.6c-16.2,0-29.7-12-29.7-29.8c0-17.7,13.4-29.8,29.8-29.8c15.9,0,28.7,11.4,29,29.8V57.1z M265,46.1h23.2c-1.7-6-6.2-8.8-11.3-8.8C271.8,37.3,266.7,40.4,265,46.1z"/><path fill="#FFFFFF" d="M342.4,80.6h-18.3l-23.3-56.3h18.9L333.3,59l13.4-34.7h18.9L342.4,80.6z"/><g><polygon fill="#FFFFFF" points="166.8,24.3 139.8,24.3 166.8,54.3 "/></g><g><polygon fill="#FFFFFF" points="139.8,80.5 166.8,80.5 139.8,50.5 "/></g><path fill="#FFFFFF" d="M57,80.5H39.1v-5.4c-4.3,4.5-10.2,7.1-17.2,7.1C8.7,82.2,0,72.9,0,58.8V24.3h17.8V55c0,6.6,3.9,11.1,9.6,11.1c7.1,0,11.7-4.8,11.7-15.7V24.3H57V80.5z"/><path fill="#FFFFFF" d="M121.7,37.8c-4.7-9.3-13.8-15.1-24.1-15.1c-7.3,0-13.1,2.2-17.5,6.2v-4.5H62.2v75.3h17.9V76.2c4.4,3.9,10.2,6.2,17.5,6.2c10.3,0,19.4-5.7,24.1-15V37.8z M93.9,67.1c-7.3,0-13.8-5.8-13.8-14.6c0-8.6,6.5-14.6,13.8-14.6c7.8,0,13.6,5.9,13.6,14.6C107.5,61.3,101.8,67.1,93.9,67.1z"/><path fill="#FFFFFF" d="M225.8,0v28.9c-4.4-3.9-10.2-6.2-17.5-6.2c-9.6,0-18.2,5.1-23.1,13.4V69c4.9,8.3,13.5,13.3,23.1,13.3c7.3,0,13.1-2.2,17.5-6.2v4.5h17.9V0H225.8z M212.1,67.1c-7.8,0-13.6-5.8-13.6-14.6c0-8.6,5.7-14.6,13.6-14.6c7.3,0,13.8,5.9,13.8,14.6C225.8,61.3,219.3,67.1,212.1,67.1z"/></g></svg>
        <h1 class="heroHeading">Intelligent solutions <span>to optimize your business</span></h1>
    </div>
    <a href="#" class="heroBtn" data-link="about">
        <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
    </a>
</section>


<section class="about">
    <h2 class="sectionHeading">Abuot us</h2>
        <div class="textBlock">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
            <p>Proin sodales pulvinar tempor. Gravida dolor sit ameumsan et viverra justo</p>
        </div>
        <div class="sertificatesText">The skill set of our certified team is the reason for our success:</div>
        <div class="sertificates">
            <div class="wrapper" style="background-image: url(images/iso.jpg)"></div>
            <div class="wrapper" style="background-image: url(images/zend.jpg)"></div>
            <div class="wrapper" style="background-image: url(images/scrum.jpg)"></div>
            <div class="wrapper" style="background-image: url(images/cua.jpg)"></div>
            <div class="wrapper" style="background-image: url(images/prince.jpg)"></div>
        </div>
</section>


<div class="logo"></div>
<div class="layout--right"></div>
<div class="layout--center"></div>
<div class="layout--left">
    <div class="sliderButtons">
        <div class="swiper-button-prev">
            <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
        </div>
        <div class="swiper-button-next">
            <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
        </div>
    </div>
</div>


<section class="services">
    <h2 class="sectionHeading">Services</h2>
    <ul class="textList">
        <li>Websites, Landing Pages, Facebook Apps</li>
        <li>Web Apps & SaaS</li>
        <li>Front-end & Back-end Development</li>
        <li>Chatbots</li>
    </ul>
    <ul class="textList">
        <li>CMS Development</li>
        <li>System Architecture & Database Design</li>
        <li>Internal SEO, Page Speed Optimization</li>
        <li>Integrations & Support</li>
    </ul>
    <aside class="rightContent">
        <a href="#" class="btn">
            <h3 class="sectionSubHeading">Skills and Capabilities</h3>
            <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
        </a>
        <div class="sertificatesText">Sertificates:</div>
        <div class="container">
            <div class="wrapper" style="background-image: url(images/iso.jpg)"><a href="#" class="link"></a></div>
            <div class="wrapper" style="background-image: url(images/zend.jpg)"></div>
            <div class="wrapper" style="background-image: url(images/scrum.jpg)"></div>
            <div class="wrapper u-mobileHide" style="background-image: url(images/cua.jpg)"></div>
        </div>
    </aside>
</section>


<section class="projects">
    <div class="sliderButtons">
        <div class="swiper-button-prev">
            <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
        </div>
        <div class="swiper-button-next">
            <svg class="arrowSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 44.2 44.2" enable-background="new 0 0 44.2 44.2" xml:space="preserve"><g><g><path  d="M22.1,44.2C9.9,44.2,0,34.3,0,22.1C0,9.9,9.9,0,22.1,0c12.2,0,22.1,9.9,22.1,22.1C44.2,34.3,34.3,44.2,22.1,44.2z M22.1,1.5c-11.4,0-20.6,9.2-20.6,20.6c0,11.4,9.3,20.6,20.6,20.6c11.4,0,20.6-9.3,20.6-20.6C42.7,10.7,33.5,1.5,22.1,1.5z"/><g><path  d="M22.1,29.3c-0.4,0-0.8-0.3-0.8-0.8V17c0-0.4,0.3-0.8,0.8-0.8c0.4,0,0.8,0.3,0.8,0.8v11.6C22.9,29,22.5,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2l-3.3-3.3c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l3.3,3.3c0.3,0.3,0.3,0.8,0,1.1C22.5,29.3,22.3,29.3,22.1,29.3z"/></g><g><path  d="M22.1,29.3c-0.2,0-0.4-0.1-0.5-0.2c-0.3-0.3-0.3-0.8,0-1.1l3.3-3.3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.3,3.3C22.5,29.3,22.3,29.3,22.1,29.3z"/></g></g></g></svg>
        </div>
    </div>
    <h2 class="sectionHeading">Projects</h2>
    <div class="slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/iholistics.jpg)"></div>
                    <h3 class="wrapperHeading">
                        iHolistics
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/g-spot-vilnius.jpg)"></div>
                    <h3 class="wrapperHeading">
                        G spot Vilnius
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/compensa.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Compensa - Palanga camping
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/WM.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Weimann & Meyer
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/smallpage.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Smallpage
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/westcoast.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Weast Coast
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/autoriai.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Autoriai
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/nicolle.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Nicolle Sassman
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/so-me.jpg)"></div>
                    <h3 class="wrapperHeading">
                        So Me
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/puglu.jpg)"></div>
                    <h3 class="wrapperHeading">
                        House of Puglu
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/mimai.jpg)"></div>
                    <h3 class="wrapperHeading">
                        London Mime - Evelina
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/tstop.jpg)"></div>
                    <h3 class="wrapperHeading">
                        T-Stop
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>
                <div class="swiper-slide">
                    <div class="projectImage" style="background-image: url(images/projects/promodiem.jpg)"></div>
                    <h3 class="wrapperHeading">
                        Promodiem
                        <span class="wrapperHeading--sub">Design by <a class="textLink" href="#">Loremip</a>, Coding by <a class="textLink" href="#">Upndev</a></span>
                    </h3>
                </div>


            </div>
        </div>
    </div>
</section>


<?php include 'includes/footer.php'; ?>